'use strict'

const Log = require('../Controllers/Http/Helper/Log.js')
const moment = require('moment')
const Logger = use('Logger')

class LogSystems {
	async handle({ params, request, response, session, auth }, next) {
		let user_id
		try {
			user_id = auth.user.id
		} catch(error) {
			user_id = 1
		}
		
		await Log.set({
			user_id: user_id,
			access: 'Backend',
			ip: request.ip(),
			user_agent: request.headers()['user-agent'],
			url: request.originalUrl(),
			method: request.method(),
			param: params,
			body: request.all(),
			response: response._lazyBody
		})
		
		await next()
	}
}

module.exports = LogSystems
