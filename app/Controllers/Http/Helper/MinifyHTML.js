'use strict'

const minifyHTMLEngine = require('html-minifier').minify
const UglifyJS = require('uglify-es')
const Env = use('Env')

class minifyHTML {
	async minify(template) {
		if (Env.get('CACHE_TEMPLATE') == 'false') {
			return template
		} else {
			let resultMinify = minifyHTMLEngine(template, {
				collapseBooleanAttributes: true,
				collapseInlineTagWhitespace: false,
				collapseWhitespace: true,
				decodeEntities: true,
				removeComments: true,
				removeCommentsFromCDATA: true,
				keepClosingSlash: true,
				minifyCSS: true,
				minifyJS: this.minifyES6,
				preserveLineBreaks: false,
			})
			return resultMinify
		}
	}
	
	minifyES6(text, inline) {
		let uglifyEsOptions = { parse: {}, ecma: 5, ie8: true, safari10: true }
		let code = text.match(/^\s*\s*$/) ? '' : text;
		uglifyEsOptions.parse.bare_returns = inline
		let result = UglifyJS.minify(code, uglifyEsOptions)
		if (result.error) {
			console.log('Uglify-es error:', result.error)
			return text
		}
		return result.code.replace(/;$/, '')
	}
}

module.exports = new minifyHTML
