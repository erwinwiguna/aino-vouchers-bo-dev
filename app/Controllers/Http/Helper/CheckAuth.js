'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const RoleUser = use('App/Models/RoleUser')
const Logger = use('Logger')
const _lo = use('lodash')

class CheckAuth {
	async get(mod, permit, auth) {
		let roleStatus = false
		let user = auth.user
		let userRoles = await RoleUser.query()
			.where('role_user.user_id', user.id)
			.leftJoin('roles', 'role_user.role_id', 'roles.id')
			.first()
		let userRolesJson = userRoles.toJSON()
		let roleState = userRolesJson.role_access == '' || userRolesJson.role_access == null ? null : JSON.parse(userRolesJson.role_access)
		let isRole = ''
		if (!roleState) {
			roleStatus = false
		} else {
			if (permit == 'create') {
				isRole = _lo.find(roleState, { 'component': mod, 'create': '1' })
			} else if (permit == 'read') {
				isRole = _lo.find(roleState, { 'component': mod, 'read': '1' })
			} else if (permit == 'update') {
				isRole = _lo.find(roleState, { 'component': mod, 'update': '1' })
			} else if (permit == 'delete') {
				isRole = _lo.find(roleState, { 'component': mod, 'delete': '1' })
			} else {
				isRole = null
			}
			if (isRole) {
				roleStatus = true
			} else {
				roleStatus = false
			}
		}
		return roleStatus
	}
}

module.exports = new CheckAuth