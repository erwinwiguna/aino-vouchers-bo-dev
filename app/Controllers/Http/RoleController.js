'use strict'

const Role = use('App/Models/Role')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const _lo = use('lodash')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class RoleController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Role', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('role.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'roles',
			sSelectSql: ["id", "role_title", "role_slug"],
			aSearchColumns: ["role_title", "role_slug"],
			dbType: 'postgres'
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows
		
		let fdata = []
		let no = 1
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['role_title'],
				selectRow[x]['role_slug'],
				"<div class='text-center'>\
					<a href='./role/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let lfiles = []
		let directoryPath = path.join(__dirname, '')
		const files = fs.readdirSync(directoryPath).map(f => f.replace('Controller.js', ''))
		lfiles.push(...files)
		
		let allFiles = []
		for (let i = 0; i < lfiles.length; i++) {
			if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error') {
			} else {
				allFiles.push(lfiles[i])
			}
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('role.create', {
			meta: meta,
			files: allFiles
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		
		let roles = []
		let items = formData.item
		for(let x in items) {
			roles.push({
				component: items[x]['component'] ? items[x]['component'] : '-',
				create: items[x]['create'] ? '1' : '0',
				read: items[x]['read'] ? '1' : '0',
				update: items[x]['update'] ? '1' : '0',
				delete: items[x]['delete'] ? '1' : '0'
			})
		}
		
		let data = {
			role_title: formData.role_title,
			role_slug: formData.role_slug,
			role_access: JSON.stringify(roles),
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			role_title: 'required|unique:roles,role_title',
			role_slug: 'required|unique:roles,role_slug'
		}
		
		const validation = await validateAll(data, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Role.create(data)
			session.flash({ notification: 'Role added', status: 'success' })
			return response.redirect('/role')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let role = await Role.find(params.id)
		
		let lfiles = []
		let directoryPath = path.join(__dirname, '')
		const files = fs.readdirSync(directoryPath).map(f => f.replace('Controller.js', ''))
		lfiles.push(...files)

		let allFiles = []
		let itemFusion = []
		let itemFusion2 = []
		let itemFusion3 = []
		let roleState = role.role_access == '' || role.role_access == null ? false : JSON.parse(role.role_access)

		if (!roleState) {
			for (let i = 0; i < lfiles.length; i++) {
				if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error') {
				} else {
					allFiles.push({
						component: lfiles[i],
						create: '',
						read: '',
						update: '',
						delete: ''
					})
				}
			}
		} else {
			for (let x in roleState) {
				if (lfiles.indexOf(roleState[x]['component']) == -1) {
					roleState.splice(roleState[x], 1)
				}
				itemFusion2.push(roleState[x])
			}
			
			for (let i = 0; i < lfiles.length; i++) {
				if (lfiles[i] == 'Helper' || lfiles[i] == 'Api' || lfiles[i] == 'Module' || lfiles[i] == 'Error') {
				} else {
					itemFusion3.push({
						component: lfiles[i],
						create: '',
						read: '',
						update: '',
						delete: ''
					})
				}
			}

			for (let x in itemFusion3) {
				let chckFus = _lo.find(itemFusion2, { component: itemFusion3[x]['component'] })
				if (_lo.size(chckFus) == 0) {
					itemFusion.push(itemFusion3[x])
				}
			}
			
			allFiles = itemFusion2.concat(itemFusion)
		}

		if (role) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('role.edit', {
				meta: meta,
				role: role,
				files: _lo.sortBy(allFiles, ['component'])
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Role not found', status: 'success' })
			return response.redirect('/role')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		
		let roles = []
		let items = formData.item
		for(let x in items) {
			roles.push({
				component: items[x]['component'] ? items[x]['component'] : '-',
				create: items[x]['create'] ? '1' : '0',
				read: items[x]['read'] ? '1' : '0',
				update: items[x]['update'] ? '1' : '0',
				delete: items[x]['delete'] ? '1' : '0'
			})
		}
		
		let role = await Role.find(params.id)
		
		let data = {
			role_title: formData.role_title,
			role_slug: formData.role_slug,
			role_access: JSON.stringify(roles),
			updated_by: auth.user.id
		}
		
		let rules = {
			role_title: `required|unique:roles,role_title,id,${role.id}`,
			role_slug: `required|unique:roles,role_slug,id,${role.id}`,
		}
		
		const validation = await validateAll(data, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (role) {
				await Role.query().where('id', params.id).update(data)
				session.flash({ notification: 'Role updated', status: 'success' })
				return response.redirect('/role')
			} else {
				session.flash({ notification: 'Role not found', status: 'danger' })
				return response.redirect('/role')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let role = await Role.find(formData.id)
		if (role){
			try {
				await role.delete()
				session.flash({ notification: 'Role success deleted', status: 'success' })
				return response.redirect('/role')
			} catch (e) {
				session.flash({ notification: 'Role cannot be delete', status: 'danger' })
				return response.redirect('/role')
			}
		} else {
			session.flash({ notification: 'Role cannot be delete', status: 'danger' })
			return response.redirect('/role')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Role', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let role = await Role.find(formData.item[i])
				try {
					await role.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Role success deleted', status: 'success' })
			return response.redirect('/role')
		} else {
			session.flash({ notification: 'Role cannot be deleted', status: 'danger' })
			return response.redirect('/role')
		}
	}
}

module.exports = RoleController