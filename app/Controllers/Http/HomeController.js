'use strict'

const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const Logger = use('Logger')
const Env = use('Env')
const moment = use('moment')
const minifyHTML = require('./Helper/MinifyHTML.js')

class HomeController {
	async getDashboard({ request, response, auth, view }) {
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('dashboard', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
}

module.exports = HomeController