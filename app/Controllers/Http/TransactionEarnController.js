'use strict'

const TransactionEarn = use('App/Models/TransactionEarn')
const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')
const moment = require('moment');
const arangojs = require("arangojs");
const ArangoService = require('./Helper/ArangoService.js')

class TransactionEarnController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('TransactionEarn', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('transaction-earn.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async datatable({request, response, auth, view, session}) {
		const formData = request.post()

		let tableDefinition

		if (auth.user.user_role == '1' || auth.user.user_role == '2') {
			tableDefinition = {
				sTableName: 'transaction_earns',
				sFromSql: "transaction_earns inner join users on transaction_earns.user_id = users.id",
				sSelectSql: ["transaction_earns.id", "user_name", "user_id", "issuer_name", "card_number", "activity_type", "spending_type", "amount", "reward_amount", "reward_type", "date", "location"],
				aSearchColumns: ["user_id" , "user_name", "issuer_name", "card_number", "activity_type", "spending_type", "amount", "reward_amount", "reward_type", "location"],
				dbType: 'postgres'
			}
		} else {
			tableDefinition = {
				sTableName: 'transaction_earns',
				sFromSql: "transaction_earns inner join users on transaction_earns.user_id = users.id",
				sSelectSql: ["transaction_earns.id", "user_name", "user_id", "issuer_name", "card_number", "activity_type", "spending_type", "amount", "reward_amount", "reward_type", "date", "location"],
				aSearchColumns: ["user_id" , "user_name", "issuer_name", "card_number", "activity_type", "spending_type", "amount", "reward_amount", "reward_type", "location"],
				sWhereAndSql: "users.principal_id = '"+ auth.user.principal_id +"'",
				dbType: 'postgres'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		for(let x in selectRow) {
			fdata.push([
				no,
				selectRow[x]['user_id'],
				selectRow[x]['user_name'],
				selectRow[x]['issuer_name'],
				selectRow[x]['card_number'],
				selectRow[x]['activity_type'],
				selectRow[x]['spending_type'],
				selectRow[x]['amount'],
				selectRow[x]['reward_amount'],
				selectRow[x]['reward_type'],
				moment(selectRow[x]['date']).format('DD/MM/YYYY'),
				moment(selectRow[x]['date']).format('HH:mm'),
				selectRow[x]['location']
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
}

module.exports = TransactionEarnController
