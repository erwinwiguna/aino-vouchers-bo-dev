'use strict'

const Merchant = use('App/Models/Merchant')
const Principal = use('App/Models/Principal')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class MerchantController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Merchant', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('merchant.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'merchants',
			sFromSql: "merchants inner join principals on principals.id = merchants.principal_id",
			sSelectSql: ['merchants.id', 'principals.name as principal_name', 'merchants.name', 'merchants.merchant_code'],
			aSearchColumns: ['principal_name', 'name', 'merchant_code'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 0
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				selectRow[x]['id'],
				selectRow[x]['principal_name'],
				selectRow[x]['name'],
				selectRow[x]['merchant_code'],
				"<div class='text-center'>\
					<a href='./merchant/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('merchant.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { principal_id, name, merchant_code } = request.only(['principal_id', 'name', 'merchant_code'])
		
		let formData = {
			principal_id: principal_id,
			name: name,
			merchant_code: merchant_code,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			principal_id: 'required',
			name: 'required',
			merchant_code: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Merchant.create(formData)
			session.flash({ notification: 'Merchant added', status: 'success' })
			return response.redirect('/merchant')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let merchant = await Merchant.find(params.id)
		let principal = await Principal.query().select('name').where('id', merchant.principal_id).first()

		if (merchant) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('merchant.edit', {
				meta: meta,
				merchant: merchant,
				principal: principal.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Merchant not found', status: 'danger' })
			return response.redirect('/merchant')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { principal_id, name, merchant_code } = request.only(['principal_id', 'name', 'merchant_code'])
		
		let merchant = await Merchant.find(params.id)

		let formData = {
			principal_id: principal_id,
			name: name,
			merchant_code: merchant_code,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		let rules = {
			principal_id: 'required',
			name: 'required',
			merchant_code: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (merchant) {
				await Merchant.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Merchant updated', status: 'success' })
				return response.redirect('/merchant')
			} else {
				session.flash({ notification: 'Merchant not found', status: 'danger' })
				return response.redirect('/merchant')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let merchant = await Merchant.find(formData.id)
		if (merchant){
			try {
				await merchant.delete()
				session.flash({ notification: 'Merchant success deleted', status: 'success' })
				return response.redirect('/merchant')
			} catch (e) {
				session.flash({ notification: 'Merchant cannot be delete', status: 'danger' })
				return response.redirect('/merchant')
			}
		} else {
			session.flash({ notification: 'Merchant cannot be delete', status: 'danger' })
			return response.redirect('/merchant')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Merchant', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let merchant = await Merchant.find(formData.item[i])
				try {
					await merchant.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Merchant success deleted', status: 'success' })
			return response.redirect('/merchant')
		} else {
			session.flash({ notification: 'Merchant cannot be deleted', status: 'danger' })
			return response.redirect('/merchant')
		}
	}

	async getMerchant({request, response, auth, view, session}) {
		let req = request.get()
		let merchants
		if (req.phrase != 'undefined') {
			merchants = await Merchant.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			merchants = await Merchant.query().limit(20).fetch()
		}
		let merchant = merchants.toJSON()
		
		let data = []
		for(let i in merchant){
			data.push({
				id: merchant[i]['id'],
				text: merchant[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = MerchantController
