'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const Token = use('App/Models/Token')
const RoleUser = use('App/Models/RoleUser')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const MasterService = require('./Helper/MasterService.js')

class UserController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('User', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('users.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition

		if (auth.user.user_role == '1') {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["id", "email", "fullname"],
				aSearchColumns: ["email", "fullname"],
				dbType: 'postgres'
			}
		} else if (auth.user.user_role == '2') {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["id", "email", "fullname"],
				aSearchColumns: ["email", "fullname"],
				sWhereAndSql: "user_role != 1",
				dbType: 'postgres'
			}
		} else {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["id", "email", "fullname"],
				aSearchColumns: ["email", "fullname"],
				sWhereAndSql: "id = "+ auth.user.id +"",
				dbType: 'postgres'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		for(let x in selectRow) {
			let role = await RoleUser.query().select('roles.role_title').leftJoin('roles', 'roles.id', 'role_user.role_id').where('role_user.user_id', selectRow[x]['id']).first()

			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['fullname'],
				selectRow[x]['email'],
				role ? role.role_title : '-',
				"<div class='text-center'>\
					<a href='./users/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let roles
		if (auth.user.id == 1) {
			roles = await Role.query().select('id', 'role_title').orderBy('role_title', 'ASC').fetch()
		} else {
			roles = await Role.query().select('id', 'role_title').where('role_slug', '!=', 'superadmin').where('role_slug', '!=', 'customer').orderBy('role_title', 'ASC').fetch()
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let principals = await MasterService.getPrincipals()
		let principal = principals['data']['data']
		let dataPrincipal = []

		for(let i in principal) {
			dataPrincipal.push({
				id: principal[i]['mid'],
				text: principal[i]['name']
			})
		}
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('users.create', {
			meta: meta,
			roles: roles.toJSON(),
			principals: dataPrincipal
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let { fullname, username, email, password, user_role, principal_id } = request.only(['fullname', 'username', 'email', 'password','user_role', 'principal_id'])
		
		let splitPrincipal = principal_id ? principal_id.split("-") : null

		let formData = {
			username: username,
			email: email,
			fullname: fullname,
			password: password,
			user_type: 1,
			user_role: user_role,
			social_token: '',
			activation_key: await Hash.make(password),
			block: 'N',
			forget_key: null,
			principal_id: splitPrincipal == null ? null : splitPrincipal[0],
			principal_code: splitPrincipal == null ? null : splitPrincipal[1].replace(/_/g, '').replace(/-/g, '').replace(/ /g, '').replace(/[.*+?^${}()|[\]\\]/g, '').toLowerCase(),
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			username: 'required|unique:users,username',
			email: 'required|email|unique:users,email',
			password: 'required',
			user_role: 'required',
		}
		
		const validation = await validateAll(formData, rules)	
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password'])
			return response.redirect('back')
		} else {
			if (auth.user.id == '1') {
				let newuser = await User.create(formData)
				await RoleUser.create({
					role_id: user_role,
					user_id: newuser.id,
					created_by: auth.user.id,
					updated_by: auth.user.id
				})
				session.flash({ notification: 'User added', status: 'success' })
				return response.redirect('/users')
			} else {
				session.flash({ notification: 'Not Authorization', status: 'danger' })
				return response.redirect('/users')
			}
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let user
		if (auth.user.id == '1') {
			user = await User.query().where('users.id', params.id).first()
		} else {
			if (auth.user.id == params.id) {
				user = await User.query().where('users.id', params.id).first()
			} else {
				user = false
			}
		}
		
		if (user) {
			let roles
			if (auth.user.id == 1) {
				roles = await Role.query().select('id', 'role_title').orderBy('role_title', 'ASC').fetch()
			} else {
				roles = await Role.query().select('id', 'role_title').where('role_slug', '!=', 'superadmin').orderBy('role_title', 'ASC').fetch()
			}

			let principals = await MasterService.getPrincipals()
			let principal = principals['data']['data']
			let dataPrincipal = []

			for(let i in principal) {
				dataPrincipal.push({
					id: principal[i]['mid'],
					text: principal[i]['name']
				})
			}
			
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('users.edit', {
				meta: meta,
				user: user,
				roles: roles.toJSON(),
				principals: dataPrincipal
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'User not found', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { fullname, username, email, password, user_role, principal_id } = request.only(['fullname', 'username', 'email', 'password', 'user_role', 'principal_id'])

		let user
		if (auth.user.id == '1') {
			user = await User.find(params.id)
		} else {
			if (auth.user.id == params.id) {
				user = await User.find(params.id)
			} else {
				user = false
			}
		}
		
		const userData = {
			fullname: fullname,
			username: username,
			email: email,
			user_role: user_role,
		}
		
		let rules = {
			username: `required|unique:users,username,id,${user.id}`,
			email: `required|unique:users,email,id,${user.id}`,
			user_role: 'required',
			fullname: 'required'
		}

		const validation = await validateAll(userData, rules)	
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password'])
			return response.redirect('back')
		} else {
			if (user) {
				let splitPrincipal = principal_id ? principal_id.split("-") : null
				
				if (password == '' || password == null) {
					await User.query().where('id', params.id).update({
						fullname: fullname,
						username: username,
						email: email,
						user_role: user_role,
						principal_id: splitPrincipal == null ? null : splitPrincipal[0],
						principal_code: splitPrincipal == null ? null : splitPrincipal[1].replace(/_/g, '').replace(/-/g, '').replace(/ /g, '').replace(/[.*+?^${}()|[\]\\]/g, '').toLowerCase(),
						updated_by: auth.user.id
					})
				} else {
					await User.query().where('id', params.id).update({
						fullname: fullname,
						username: username,
						email: email,
						password: await Hash.make(password),
						user_role: user_role,	
						principal_id: splitPrincipal == null ? null : splitPrincipal[0],
						principal_code: splitPrincipal == null ? null : splitPrincipal[1].replace(/_/g, '').replace(/-/g, '').replace(/ /g, '').replace(/[.*+?^${}()|[\]\\]/g, '').toLowerCase(),
						updated_by: auth.user.id
					})
				}
				await RoleUser.query().where('user_id', params.id).update({
					role_id: user_role,
					user_id: params.id,
					updated_by: auth.user.id
				})
				session.flash({ notification: 'User updated', status: 'success' })
				return response.redirect('/users')
			} else {
				session.flash({ notification: 'User not found', status: 'danger' })
				return response.redirect('/users')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if(auth.user.id != formData.id){
			let users = await User.query().where('id', formData.id).first()
			let tokens = await Token.query().where('user_id', users.id).first()
			if (users != 1) {
				try{
					if(tokens) {
						await tokens.delete()
					}
					await users.delete()
				}catch(e){
					console.log(e)
				}

			session.flash({ notification: 'User deleted', status: 'success' })
			return response.redirect('/users')
			} else {
				session.flash({ notification: 'User cannot be deleted', status: 'danger' })
				return response.redirect('/users')
			}
		} else {
			session.flash({ notification: 'User cannot be deleted', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				if (formData.item[i] != auth.user.id) {
					let users = await User.find(formData.item[i])

					if (auth.user.id != formData.item[i]) {
						try {
							let tokens = await Token.query().where('user_id', users.id).first()
							
							if(tokens) {
								await tokens.delete()
							}
							await users.delete()
						} catch (e) {
							console.log(e)
						}
					} else {
						session.flash({ notification: 'User cannot be deleted', status: 'danger' })
						return response.redirect('/users')
					}
				}
			}
			session.flash({ notification: 'User success deleted', status: 'success' })
			return response.redirect('/users')
		} else {
			session.flash({ notification: 'User cannot be deleted', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async getUser({request, response, auth, view, session}) {
		let req = request.get()
		let users
		if (req.phrase != 'undefined') {
			users = await User.query().select('users.id', 'users.fullname', 'users.email').where('users.fullname', 'LIKE', '%' + req.phrase + '%').orWhere('users.email', 'LIKE', '%' + req.phrase + '%').limit(20).orderBy('users.id', 'DESC').fetch()
		} else {
			users = await User.query().select('users.id', 'users.fullname', 'users.email').limit(20).orderBy('users.id', 'DESC').fetch()
		}
		let user = users.toJSON()
		
		let data = []
		for(let i in user){
			data.push({
				id: user[i]['id'],
				text: user[i]['fullname'] + ' (' + user[i]['email'] + ')'
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = UserController