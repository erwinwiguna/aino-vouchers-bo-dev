'use strict'

const User = use('App/Models/User')
const minifyHTML = require('./Helper/MinifyHTML.js')

class AccountController {
	async getIndex({ request, response, auth, view }) {
		let checkAuth = await this.checkAuth(auth)
		if (checkAuth) {
			return response.redirect('/home')
		} else {
			let template = view.render('welcome')
			
			return await minifyHTML.minify(template)
		}
	}

	async getLogin({ request, response, auth, view }) {
		let checkAuth = await this.checkAuth(auth)
		if (checkAuth) {
			return response.redirect('/home')
		} else {
			let template = view.render('welcome')
			
			return await minifyHTML.minify(template)
		}
	}

	async postLogin({ request, response, auth, view, session }) {
		const { email, password } = request.all()
	 	try {
			let checkusertype = await User.query().select('id', 'email', 'user_type').where('email', email).where('user_type', '1').first()
			if (checkusertype) {
				const login = await auth.attempt(email, password)
				if (login) {
					const user = await auth.getUser()
					let get_roles = await user.roles().fetch()
					let users_roles_json = get_roles.toJSON()
					let users_roles = []
					for(let i in users_roles_json){
						users_roles.push(users_roles_json[i].role_slug)
					}
					session.flash({ users_roles: users_roles })
					return response.redirect('/home')
				} else {
					session.flash({ notification: 'Cannot verify user' })
					return response.redirect('/')
				}
			} else {
				session.flash({ notification: 'Cannot verify user' })
				return response.redirect('/')
			}
		} catch (e) {
			session.flash({ notification: 'Cannot verify user' })
			return response.redirect('/')
		}
	}

	async getLogout({ request, response, auth }) {
		await auth.logout()
		return response.redirect('/')
	}
	
	async checkAuth(auth) {
		try {
			return await auth.check()
		} catch(error) {
			return false
		}
	}
	
	async checkUser(auth) {
		try {
			return await auth.getUser()
		} catch(error) {
			return false
		}
	}
}

module.exports = AccountController