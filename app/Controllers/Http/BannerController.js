'use strict'

const Banner = use('App/Models/Banner')
const Reward = use('App/Models/Reward')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const codeGen = require('./Helper/CodeGenerator.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment')
const Helpers = use('Helpers')

class BannerController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Banner', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('banner.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()

		let tableDefinition

		if (auth.user.user_role == '1' || auth.user.user_role == '2') {
			tableDefinition = {
				sTableName: 'banners',
				sSelectSql: ["id", "title", "start_date", "start_hour", "end_date", "end_hour", "picture", "link", "status"],
				aSearchColumns: ["title", "picture", "link"],
				dbType: 'postgres'
			}
		} else {
			tableDefinition = {
				sTableName: 'banners',
				sSelectSql: ["id", "title", "start_date", "start_hour", "end_date", "end_hour", "picture", "link", "status", "principal_id"],
				aSearchColumns: ["title", "picture", "link"],
				sWhereAndSql: "principal_id = '"+ auth.user.principal_id +"'",
				dbType: 'postgres'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows
		
		let fdata = []
		let no = 1
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['title'],
				moment(selectRow[x]['start_date']).format('DD/MM/YYYY') + '<br>' + moment(selectRow[x]['start_hour'], 'HH:mm:ss').format('HH:mm'),
				moment(selectRow[x]['end_date']).format('DD/MM/YYYY') + '<br>' + moment(selectRow[x]['end_hour'], 'HH:mm:ss').format('HH:mm'),
				selectRow[x]['picture'] ? "<img style='max-width:50px; max-height:50px;' src='"+selectRow[x]['picture']+"' alt='"+selectRow[x]['title']+"' />" : '-',
				selectRow[x]['link'],
				selectRow[x]['status'] == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Non Active</span>",
				"<div class='text-center'>\
					<a href='./banner/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('banner.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { title, status, start_date, start_hour, end_date, end_hour, link, reward_id } = request.only(['title', 'status', 'start_date', 'start_hour', 'end_date', 'end_hour', 'link', 'reward_id'])
		
		let nameBanner

		const bannerPic = request.file('picture', {
			types: ['image'],
			size: '1024mb'
		})

		if(bannerPic != null) {
			const type = request.file('picture').subtype;
			nameBanner = `${new Date().getTime()}_banner_${auth.user.id}.${type}`;
			
			await bannerPic.move(Helpers.publicPath('/uploads/images/banner'), {
				name: nameBanner
			})

			if (!bannerPic.moved()) {
				return bannerPic.error()
			}
		}

		let formData = {
			user_id: auth.user.id,
			principal_id: auth.user.principal_id,
			principal_code: auth.user.principal_code,
			title: title,
			status: parseInt(status),
			start_date: start_date,
			start_hour: start_hour,
			end_date: end_date,
			end_hour: end_hour,
			picture: '/uploads/images/banner/' + nameBanner,
			link: link,
			reward_id: reward_id ? reward_id : null,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			title: 'required',
			status: 'required',
			start_date: 'required',
			start_hour: 'required',
			end_date: 'required',
			end_hour: 'required',
			link: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Banner.create(formData)
			session.flash({ notification: 'Banner added', status: 'success' })
			return response.redirect('/banner')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let banner = await Banner.find(params.id)
		banner['start_date'] = moment(banner.start_date).format('YYYY-MM-DD')
		banner['end_date'] = moment(banner.end_date).format('YYYY-MM-DD')

		if (banner) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

			let reward

			if (auth.user.user_role == '1' || auth.user.user_role == '2') {
				reward = await Reward.query()
					.select('rewards.id', 'rewards.name')
					.where('rewards.status', 1)
					.orderBy('rewards.id', 'asc')
					.fetch()
			} else {
				reward = await Reward.query()
					.select('rewards.id', 'rewards.name')
					.leftJoin('loyalty_details', 'loyalty_details.id', 'rewards.loyalty_detail_id')
					.leftJoin('loyalties', 'loyalties.id', 'loyalty_details.loyalty_id')
					.where('loyalties.principal_id', auth.user.principal_id)
					.where('rewards.status', 1)
					.orderBy('rewards.id', 'asc')
					.fetch()
			}
		
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('banner.edit', {
				meta: meta,
				banner: banner,
				rewards: reward.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Banner not found', status: 'danger' })
			return response.redirect('/banner')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { title, status, start_date, start_hour, end_date, end_hour, link, reward_id } = request.only(['title', 'status', 'start_date', 'start_hour', 'end_date', 'end_hour', 'link', 'reward_id'])
		
		let banner = await Banner.find(params.id)
		
		let formData = {
			user_id: auth.user.id,
			title: title,
			status: parseInt(status),
			start_date: start_date,
			start_hour: start_hour,
			end_date: end_date,
			end_hour: end_hour,
			link: link,
			reward_id: reward_id ? reward_id : null,
			updated_by: auth.user.id
		}
		
		const rules = {
			title: 'required',
			status: 'required',
			start_date: 'required',
			start_hour: 'required',
			end_date: 'required',
			end_hour: 'required',
			link: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (banner) {
				await Banner.query().where('id', params.id).update(formData)

				let nameBanner

				const bannerPic = request.file('picture', {
					types: ['image'],
					size: '1024mb'
				})

				if(bannerPic != null) {
					const type = request.file('picture').subtype;
					nameBanner = `${new Date().getTime()}_banner_${auth.user.id}.${type}`;
					
					await bannerPic.move(Helpers.publicPath('/uploads/images/banner'), {
						name: nameBanner
					})

					if (!bannerPic.moved()) {
						return bannerPic.error()
					}
				}

				if(bannerPic != null) {
					let formDataImage = {
						picture: '/uploads/images/banner/' + nameBanner,
					}
					await Banner.query().where('id', params.id).update(formDataImage)
				}

				session.flash({ notification: 'Banner updated', status: 'success' })
				return response.redirect('/banner')
			} else {
				session.flash({ notification: 'Banner not found', status: 'danger' })
				return response.redirect('/banner')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		let banner = await Banner.find(formData.id)
		if (banner){
			try {
				await banner.delete()
				session.flash({ notification: 'Banner success deleted', status: 'success' })
				return response.redirect('/banner')
			} catch (e) {
				session.flash({ notification: 'Banner cannot be delete', status: 'danger' })
				return response.redirect('/banner')
			}
		} else {
			session.flash({ notification: 'Banner cannot be delete', status: 'danger' })
			return response.redirect('/banner')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Banner', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let banner = await Banner.find(formData.item[i])
				try {
					await banner.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Banner success deleted', status: 'success' })
			return response.redirect('/banner')
		} else {
			session.flash({ notification: 'Banner cannot be deleted', status: 'danger' })
			return response.redirect('/banner')
		}
	}
}

module.exports = BannerController