'use strict'

const Card = use('App/Models/Card')
const Issuer = use('App/Models/Issuer')
const Merchant = use('App/Models/Merchant')
const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')
const moment = require('moment');

class CardController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Card', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('card.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'cards',
			sFromSql: "cards inner join users on cards.user_id = users.id inner join issuers on cards.issuer_id = issuers.id",
			sSelectSql: ['cards.id', 'users.fullname', 'issuers.name', 'cards.card_number'],
			aSearchColumns: ['users.fullname', 'cards.name', 'cardscard_number'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 0
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				selectRow[x]['id'],
				selectRow[x]['fullname'],
				selectRow[x]['name'],
				selectRow[x]['card_number'],
				"<div class='text-center'>\
					<a href='./card/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('card.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { user_id, issuer_id, card_number } = request.only(['user_id', 'issuer_id', 'card_number'])
		
		let formData = {
			user_id: user_id,
			issuer_id: issuer_id,
			card_number: card_number,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			user_id: 'required',
			issuer_id: 'required',
			card_number: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Card.create(formData)
			session.flash({ notification: 'Card added', status: 'success' })
			return response.redirect('/card')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let card = await Card.find(params.id)

		if (card) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}

			let issuer = await Issuer.query().where('id', card.issuer_id).first()
			let user = await User.query().where('id', card.user_id).first()
			
			let template = view.render('card.edit', {
				meta: meta,
				card: card,
				issuer: issuer.toJSON(),
				user: user.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Card not found', status: 'danger' })
			return response.redirect('/card')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { user_id, issuer_id, card_number } = request.only(['user_id', 'issuer_id', 'card_number'])

		let card = await Card.find(params.id)
		
		let formData = {
			user_id: user_id,
			issuer_id: issuer_id,
			card_number: card_number,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			user_id: 'required',
			issuer_id: 'required',
			card_number: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (card) {
				await Card.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Card updated', status: 'success' })
				return response.redirect('/card')
			} else {
				session.flash({ notification: 'Card not found', status: 'danger' })
				return response.redirect('/card')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let card = await Card.find(formData.id)
		if (card){
			try {
				await card.delete()
				session.flash({ notification: 'Card success deleted', status: 'success' })
				return response.redirect('/card')
			} catch (e) {
				session.flash({ notification: 'Card cannot be delete', status: 'danger' })
				return response.redirect('/card')
			}
		} else {
			session.flash({ notification: 'Card cannot be delete', status: 'danger' })
			return response.redirect('/card')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Card', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let card = await Card.find(formData.item[i])
				try {
					await card.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Card success deleted', status: 'success' })
			return response.redirect('/card')
		} else {
			session.flash({ notification: 'Card cannot be deleted', status: 'danger' })
			return response.redirect('/card')
		}
	}

	async getIssuer({request, response, auth, view, session}) {
		let req = request.get()
		let cards
		if (req.phrase != 'undefined') {
			cards = await Card.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			cards = await Card.query().limit(20).fetch()
		}
		let card = cards.toJSON()
		
		let data = []
		for(let i in card){
			data.push({
				id: card[i]['id'],
				text: card[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = CardController
