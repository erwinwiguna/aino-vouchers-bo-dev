'use strict'

const Issuer = use('App/Models/Issuer')
const Rule = use('App/Models/Rule')
const User = use('App/Models/User')
const Merchant = use('App/Models/Merchant')
const Principal = use('App/Models/Principal')
const Reward = use('App/Models/Reward')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Helpers = use('Helpers')
const Logger = use('Logger')
const TadaService = require('./Helper/TadaService.js')
const MasterService = require('./Helper/MasterService.js')
const _lo = require('lodash')
const moment = require('moment')

class RuleController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Rule', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('rule.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		let tableDefinition

		let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
		let principals = principal.toJSON()

		if (auth.user.id == 1) {
				tableDefinition = {
				sTableName: 'rules',
				sSelectSql: ['rules.id', 'rules.name', 'rules.activity_type', 'rules.spending_type', 'rules.spending_amount', 'rules.reward_type', 'rules.reward_amount', 'rules.period_start', 'rules.period_end', 'rules.customer', 'rules.status'],
				aSearchColumns: ['rules.name', 'rules.activity_type', 'rules.spending_type', 'rules.spending_amount', 'rules.reward_type', 'rules.reward_amount', 'rules.period_start', 'rules.period_end', 'rules.customer', 'rules.status'],
				dbType: 'postgres',
			}
		} else {
				tableDefinition = {
				sTableName: 'rules',
				sSelectSql: ['rules.id', 'rules.name', 'rules.activity_type', 'rules.spending_type', 'rules.spending_amount', 'rules.reward_type', 'rules.reward_amount', 'rules.period_start', 'rules.period_end', 'rules.customer', 'rules.status'],
				aSearchColumns: ['rules.name', 'rules.activity_type', 'rules.spending_type', 'rules.spending_amount', 'rules.reward_type', 'rules.reward_amount', 'rules.period_start', 'rules.period_end', 'rules.customer', 'rules.status'],
				sFromSql: 'rules INNER JOIN users ON rules.principal_id=users.principal_id',
				sWhereAndSql: 'users.id ='+auth.user.id,
				dbType: 'postgres',
			}
		}
		
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		let start_date
		let start_hour
		let end_date
		let end_hour
		for(let x in selectRow) {
			//period start
			if(selectRow[x]['period_start'] == null || selectRow[x]['period_start'] == ""){
				start_date=""
				start_hour=""
			}else{
				start_date= moment(selectRow[x]['period_start'], 'DD/MM/YYYY').format('DD/MM/YYYY')
				start_hour= moment(selectRow[x]['period_start'], 'HH:mm').format('HH:mm')
			}
			//period end
			if(selectRow[x]['period_end'] == null || selectRow[x]['period_end'] == ""){
				end_date=""
				end_hour=""
			}else{
				end_date= moment(selectRow[x]['period_end'], 'DD/MM/YYYY').format('DD/MM/YYYY')
				end_hour= moment(selectRow[x]['period_end'], 'HH:mm').format('HH:mm')
			}

			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				selectRow[x]['name'],
				selectRow[x]['activity_type'],
				selectRow[x]['spending_type'],
				'Rp '+ parseInt(selectRow[x]['spending_amount']).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
				selectRow[x]['reward_type'],
				parseInt(selectRow[x]['reward_amount']).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."),
				start_date,
				start_hour,
				end_date,
				end_hour,
				selectRow[x]['customer'],
				selectRow[x]['status'] == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Non Active</span>",
				"<div class='text-center'>\
					<a href='./rule/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}

		let issuers = await MasterService.getIssuers()
		let issuer = issuers['data']['data']
		let dataIssuer = []

		for(let i in issuer) {
			dataIssuer.push({
				issuer_id: issuer[i]['issuer_emoney_id'],
				//issuer_id: issuer[i]['issuer_emoney_code'],
				issuer_name: issuer[i]['issuer_emoney_name']
			})
		}
		
		let template = view.render('rule.create', {
			meta: meta,
			issuer: dataIssuer
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

			const { rule_name, activity_type, spending_type, spending_amount, reward_type, reward_amount, start_date, end_date, issuer_id, customer, status, start_hour, end_hour } = request.only(['rule_name', 'activity_type', 'spending_type', 'spending_amount', 'reward_type', 'reward_amount', 'start_date', 'end_date', 'issuer_id', 'customer', 'status', 'start_hour', 'end_hour'])
			
			let start = moment(start_date+' '+start_hour, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
			let end = moment(end_date+' '+end_hour, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')

			let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
			let principals = principal.toJSON()

			let formData = {
				name: rule_name, 
				activity_type: activity_type, 
				spending_type: spending_type, 
				spending_amount: spending_amount, 
				reward_type: reward_type, 
				reward_amount: reward_amount, 
				period_start: start, 
				period_end: end, 
				issuer_id: issuer_id, 
				customer: customer, 
				status: status,
				principal_id: principals.principal_id,
				created_by: auth.user.id,
				updated_by: auth.user.id
			}
			
			const rules = {
				name: 'required',
				activity_type: 'required',
				spending_type: 'required',
				spending_amount: 'required',
				reward_type: 'required',
				reward_amount: 'required',
				period_start: 'required',
				period_end: 'required',
				issuer_id: 'required',
				customer: 'required',
				status: 'required',
			}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
			let principals = principal.toJSON()
			let rulename = await Rule.query().where('principal_id', principals.principal_id).where('name', rule_name).first()
				
			if(rulename == null || rulename == ''){
				try{
					await Rule.create(formData)
				} catch(e){
					console.log(e)
				}
				
			} else {
				session.flash({ notification: 'Rule name already exist!', status: 'danger' })
				return response.redirect('back')
			}
			session.flash({ notification: 'Rule added', status: 'success' })
			return response.redirect('/rule')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let rule = await Rule.find(params.id)

		if (rule) {
			let issuer_replace = rule.issuer_id.replace('{', '').replace('}', '').replace(/"/g, '')
			let issuers = await MasterService.getIssuers()
			let issuer = issuers['data']['data']
			let dataIssuer = []

			for(let i in issuer) {
				dataIssuer.push({
					issuer_id: issuer[i]['issuer_emoney_id'],
					issuer_name: issuer[i]['issuer_emoney_name']
				})
			}

			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}

			let getstarthour = moment(rule.period_start).format('HH:mm')
			let getendhour = moment(rule.period_end).format('HH:mm')
			let start_date = moment(rule.period_start, 'YYYY-MM-DD').format('MM-DD-YYYY')
			let end_date = moment(rule.period_end, 'YYYY-MM-DD').format('MM-DD-YYYY')
			let date_now = moment().format('MM-DD-YYYY')
			
			let template = view.render('rule.edit', {
				meta: meta,
				rule: rule,
				rule_issuers: issuer_replace,
				issuers: dataIssuer,
				getstarthour: getstarthour,
				getendhour: getendhour,
				date_now: date_now,
				start_date: start_date,
				end_date: end_date,

			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Rule not found', status: 'danger' })
			return response.redirect('/rule')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { rule_name, activity_type, spending_type, spending_amount, reward_type, reward_amount, start_date, end_date, issuer_id, customer, status, start_hour, end_hour, ruleid } = request.only(['rule_name', 'activity_type', 'spending_type', 'spending_amount', 'reward_type', 'reward_amount', 'start_date', 'end_date', 'issuer_id', 'customer', 'status', 'start_hour', 'end_hour', 'ruleid'])
		
		let rule = await Rule.find(params.id)
		let start = moment(start_date+' '+start_hour, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
		let end = moment(end_date+' '+end_hour, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
		let date_now = moment().format('YYYY-MM-DD')

		let formData = {
			name: rule_name, 
			activity_type: activity_type, 
			spending_type: spending_type, 
			spending_amount: spending_amount, 
			reward_type: reward_type, 
			reward_amount: reward_amount,
			issuer_id: issuer_id, 
			customer: customer, 
			status: status,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}

		if(start_date == 'undefined' || start == 'Invalid date'){
		} else {
			formData.period_start = start
		}
		if(moment(rule.period_end).format('YYYY-MM-DD HH:mm') != end){
			formData.period_end = end
		}
		
		const rules = {
			name: 'required',
			activity_type: 'required',
			spending_type: 'required',
			spending_amount: 'required',
			reward_type: 'required',
			reward_amount: 'required',
			issuer_id: 'required',
			customer: 'required',
			status: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (rule) {
				try{
					await Rule.query().where('id', params.id).update(formData)
					session.flash({ notification: 'Rule updated', status: 'success' })
					return response.redirect('/rule')
				}catch(e){
					console.log(e)
				}
			} else {
				session.flash({ notification: 'Rule not found', status: 'danger' })
				return response.redirect('/rule')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let rule = await Rule.find(formData.id)
		if (rule){
			try {
				await rule.delete()
				session.flash({ notification: 'Rule success deleted', status: 'success' })
				return response.redirect('/rule')
			} catch (e) {
				session.flash({ notification: 'Rule cannot be delete', status: 'danger' })
				return response.redirect('/rule')
			}
		} else {
			session.flash({ notification: 'Rule cannot be delete', status: 'danger' })
			return response.redirect('/rule')		
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Rule', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let rule = await Rule.find(formData.item[i])
				try {
					await rule.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Rule success deleted', status: 'success' })
			return response.redirect('/rule')
		} else {
			session.flash({ notification: 'Rule cannot be deleted', status: 'danger' })
			return response.redirect('/rule')
		}
	}

	async getIssuer({request, response, auth, view, session}) {
		let req = request.get()
		let rules
		if (req.phrase != 'undefined') {
			rules = await Rule.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			rules = await Rule.query().limit(20).fetch()
		}
		let rule = rules.toJSON()
		
		let data = []
		for(let i in rule){
			data.push({
				id: rule[i]['id'],
				text: rule[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async checkexist({request, response, auth, view, session}) {
		const formData = request.all()
		let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
		let principals = principal.toJSON()
		let checkexist = await Rule.query().where('principal_id', principals.principal_id).where('name', formData.rule_name).first()

		if(checkexist){
			return 1
		} else {
			return 0
		}
	}

	async checkexistedit({request, response, auth, view, session}) {
		const formData = request.all()
		let principal = await User.query().select('principal_id').where('id', auth.user.id).first()
		let principals = principal.toJSON()
		let checkexist = await Rule.query().where('name', formData.rule_name).whereNot('id', '=', formData.ruleid).where('principal_id', principals.principal_id)
		
		if(checkexist == "" || checkexist == null){
			return 0
		} else {
			return 1
		}
	}
}

module.exports = RuleController
