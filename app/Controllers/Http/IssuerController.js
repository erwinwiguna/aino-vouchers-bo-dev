'use strict'

const Issuer = use('App/Models/Issuer')
const Merchant = use('App/Models/Merchant')
const Principal = use('App/Models/Principal')
const Reward = use('App/Models/Reward')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')
const moment = require('moment');

class IssuerController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Issuer', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('issuer.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'issuers',
			sSelectSql: ['id', 'name', 'description', 'image'],
			aSearchColumns: ['name', 'description', 'image'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 0
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				selectRow[x]['id'],
				selectRow[x]['name'],
				selectRow[x]['description'],
				selectRow[x]['image'],
				"<div class='text-center'>\
					<a href='./issuer/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('issuer.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const pic = request.file('image', {
		    types: ['image'],
		    size: '5mb'
		})

		let date = moment().format("DD/MM/YYYY");
		let cn = pic.clientName
		let name_pic = date+'-'+cn
		let fix = name_pic.replace(/\//g, "")


		await pic.move(Helpers.publicPath('uploads/issuer'), {
		    name: fix
		})

		const { name, description, image } = request.only(['name', 'description', 'image'])
		
		let formData = {
			name: name,
			description: description,
			image: fix,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			name: 'required',
			description: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Issuer.create(formData)
			session.flash({ notification: 'Issuer added', status: 'success' })
			return response.redirect('/issuer')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let issuer = await Issuer.find(params.id)

		if (issuer) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('issuer.edit', {
				meta: meta,
				issuer: issuer,
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'issuer not found', status: 'danger' })
			return response.redirect('/issuer')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const pic = request.file('image', {
		    types: ['image'],
		    size: '5mb'
		})

		let date = moment().format("DD/MM/YYYY");
		let cn = pic.clientName
		let name_pic = date+'-'+cn
		let fix = name_pic.replace(/\//g, "")

		const { name, description, image } = request.only(['name', 'description', 'image'])

		let issuer = await Issuer.find(params.id)
		
		if(issuer.image != fix){
			await pic.move(Helpers.publicPath('uploads/issuer'), {
			    name: fix
			})
		}
		let formData = {
			name: name,
			description: description,
			image: fix,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			name: 'required',
			description: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (issuer) {
				await Issuer.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Issuer updated', status: 'success' })
				return response.redirect('/issuer')
			} else {
				session.flash({ notification: 'Issuer not found', status: 'danger' })
				return response.redirect('/issuer')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let issuer = await Issuer.find(formData.id)
		if (issuer){
			try {
				await issuer.delete()
				session.flash({ notification: 'Issuer success deleted', status: 'success' })
				return response.redirect('/issuer')
			} catch (e) {
				session.flash({ notification: 'Issuer cannot be delete', status: 'danger' })
				return response.redirect('/issuer')
			}
		} else {
			session.flash({ notification: 'Issuer cannot be delete', status: 'danger' })
			return response.redirect('/issuer')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Issuer', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let issuer = await Issuer.find(formData.item[i])
				try {
					await issuer.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Issuer success deleted', status: 'success' })
			return response.redirect('/issuer')
		} else {
			session.flash({ notification: 'Issuer cannot be deleted', status: 'danger' })
			return response.redirect('/issuer')
		}
	}

	async getIssuer({request, response, auth, view, session}) {
		let req = request.get()
		let issuers
		if (req.phrase != 'undefined') {
			issuers = await Issuer.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			issuers = await Issuer.query().limit(20).fetch()
		}
		let issuer = issuers.toJSON()
		
		let data = []
		for(let i in issuer){
			data.push({
				id: issuer[i]['id'],
				text: issuer[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = IssuerController
