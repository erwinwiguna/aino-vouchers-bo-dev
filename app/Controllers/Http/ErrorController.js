'use strict'

const Database = use('Database')
const Env = use('Env')
const minifyHTML = require('./Helper/MinifyHTML.js')

class ErrorController {
	async index({request, response, auth, view}) {
		let template = view.render('error.404')
		
		return await minifyHTML.minify(template)
	}
}

module.exports = ErrorController