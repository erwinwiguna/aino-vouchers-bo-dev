'use strict'

const User = use('App/Models/User')
const Customer = use('App/Models/Customer')
const UserPoint = use('App/Models/UserPoint')
const Token = use('App/Models/Token')
const Hash = use('Hash')
const Database = use('Database')
const moment = require('moment')
const { validate, validateAll, rule } = use('Validator')
const Mail = use('Mail')
const Encryption = require('crypto')
const Logger = use('Logger')
const ApiService = require('../Helper/ApiService.js')
const couponCodes = require('../Helper/CodeGenerator.js')
const Env = use('Env')

class AccountController {
	async postLogin({ request, response, auth, session }) {
		const { email, password } = request.all()
		console.log(email +'|/'+password)
	 	try {
	 		let checkifcustomer = await User.query().select('id').where('email', email).orWhere('telephone', email).where('user_type', 2).where('block', 'N').first()
	 		let setLogin 
	 		if(checkifcustomer) {
				setLogin = await auth.attempt(email, password)
			} else {
				setLogin = false
			}

		    if (setLogin) {
				let customer = await Customer.query()
					.select(
						'customers.id',
						'customers.user_id',
						'customers.gender',
						'customers.phone_number',
						'customers.date_of_birth',
						'customers.identity_number',
						'customers.address',
						'customers.image'
					)
					.where('customers.user_id', setLogin.id)
					.first()

				let user_point = await UserPoint.query().where('user_id', setLogin.id).first()
				
				if (customer) {
					let bearer = await auth.authenticator('jwt').generate(auth.user)
					let getToken = await Token.query().where('user_id', setLogin.id).getCount()
					if (getToken == 0) {
						let resetToken = await Token.create({
							user_id: setLogin.id,
							token: bearer.token,
							forever: 1,
							is_revoked: 0,
							expiry: null
						})
					} else {
						await Token.query().where('user_id', setLogin.id).update({
							token: bearer.token,
							forever: 1,
							is_revoked: 0,
							expiry: null
						})
					}

					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '2000',
						message: 'User successfull login',
						data: {
							token: bearer,
							user: setLogin,
							customer: customer,
							user_point: user_point ? user_point : null
						}
					}
					return response.send(data)
				} else {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4003',
						message: 'User not customer type',
						data: []
					}
					return response.send(data)
				}
		    } else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4005',
					message: 'User cannot verify',
					data: []
				}
				return response.send(data)
		    }
		} catch (e) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			console.log(e)
			let data = {
				code: '4004',
				message: 'User login wrong',
				data: []
			}
			return response.send(data)
		}
	}
	
	async postLoginSocial({ request, response, auth, session }) {
		const { email, fullname, social_token, login_source } = request.all()
		
		let checkuser = await User.query().where('email', email).first()
		
		if (checkuser) {
			try {
				let checkifcustomer = await User.query().select('id').where('email', email).where('user_type', 2).where('block', 'N').first()
		 		let setLogin

		 		if(checkifcustomer) {
					setLogin = await auth.login(checkuser)
				} else {
					setLogin = await auth.login([])
				}

				if (setLogin) {
					let customer = await Customer.query()
						.select(
							'customers.id',
							'customers.user_id',
							'customers.gender',
							'customers.phone_number',
							'customers.date_of_birth',
							'customers.identity_number',
							'customers.address',
							'customers.image'
						)
						.where('customers.user_id', setLogin.id)
						.first()

					let user_point = await UserPoint.query().where('user_id', setLogin.id).first()
					
					if (customer) {
						let bearer = await auth.authenticator('jwt').generate(auth.user)
						let getToken = await Token.query().where('user_id', setLogin.id).getCount()
						if (getToken == 0) {
							let resetToken = await Token.create({
								user_id: setLogin.id,
								token: bearer.token,
								forever: 1,
								is_revoked: 0,
								expiry: null
							})
						} else {
							await Token.query().where('user_id', setLogin.id).update({
								token: bearer.token,
								forever: 1,
								is_revoked: 0,
								expiry: null
							})
						}
						
						await User.query().where('id', setLogin.id).update({
							social_token: social_token,
							login_source: login_source
						})
						
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '2000',
							message: 'User successfull login',
							data: {
								token: bearer,
								user: setLogin,
								customer: customer,
								user_point: user_point ? user_point : null
							}
						}
						return response.send(data)
					} else {
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4003',
							message: 'User not customer type',
							data: []
						}
						return response.send(data)
					}
				} else {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'User cannot verify',
						data: []
					}
					return response.send(data)
				}
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User login wrong',
					data: []
				}
				return response.send(data)
			}
		} else {
			let create_date = moment()

			let formData = {
				user_id: null,
				phone_number: telephone,
				gender: null,
				date_of_birth: null,
				identity_number: null,
				address: null,
				image: null,
				created_by: '1',
				updated_by: '1',
				created_at: create_date.format('YYYY-MM-DD hh:mm:ss'),
				updated_at: create_date.format('YYYY-MM-DD hh:mm:ss')	
			}

			let formDataUser = {
				username: email,
				email: email,
				fullname: fullname,
				password: '12345678',
				user_type: '2',
				user_role: '9',
				social_token: social_token,
				activation_key: null,
				block: 'N',
				forget_key: null,
				created_by: '1',
				updated_by: '1',
				created_at: create_date.format('YYYY-MM-DD hh:mm:ss'),
				updated_at: create_date.format('YYYY-MM-DD hh:mm:ss')
			}

			const trx = await Database.beginTransaction()
			
			try {
				let userCreate = await User.create(formDataUser, trx)
				formData.user_id = userCreate.id
				
				await Customer.create(formData, trx)
								
				trx.commit()
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2001',
					message: 'User created',
					data: userCreate.toJSON()
				}
				return response.send(data)
			} catch (err) {
				trx.rollback()
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: 'User not created',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async postRegister({ request, response, auth, session }) {
		let create_date = moment()

		const {
			fullname,
			telephone,
			email,
			password,
		} = request.only([
			'fullname',
			'telephone',
			'email',
			'password',
		])

		let formData = {
			user_id: null,
			phone_number: telephone,
			gender: null,
			date_of_birth: null,
			identity_number: null,
			address: null,
			image: null,
			created_by: '1',
			updated_by: '1',
			created_at: create_date.format('YYYY-MM-DD hh:mm:ss'),
			updated_at: create_date.format('YYYY-MM-DD hh:mm:ss'),			
		}

		const rules = {
			phone_number: 'required',
		}
		
		const validation = await validate(formData, rules)
		
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: validation.messages(),
				data: []
			}
			return response.send(data)
		} else {
			let passwords = password.toString();
			let activation = await Hash.make(passwords)
			
			let formDataUser = {
				username: email,
				email: email,
				fullname: fullname,
				password: password,
				user_type: '2',
				user_role: '9',
				social_token: '',
				activation_key: Encryption.createHash('md5').update(activation).digest("hex"),
				block: 'N',
				forget_key: null,
				created_by: '1',
				updated_by: '1',
				created_at: create_date.format('YYYY-MM-DD hh:mm:ss'),
				updated_at: create_date.format('YYYY-MM-DD hh:mm:ss')
			}

			const rulesUser = {
				username: 'required|unique:users,username',
				email: 'required|email|unique:users,email',
				password: [
					rule('required'),
					rule('min', 6),
					rule('max', 20),
					// rule('regex', /[^A-Za-z0-9]/),
				],
				user_type: 'required',
				user_role: 'required',
			}

			const validationUser = await validate(formDataUser, rulesUser)
			
			if (validationUser.fails()) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: validationUser.messages(),
					data: []
				}
				return response.send(data)
			} else {
				const trx = await Database.beginTransaction()
				
				try {
					let userCreate = await User.create(formDataUser, trx)
					formData.user_id = userCreate.id
					
					await Customer.create(formData, trx)
					
					trx.commit()

					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '2000',
						message: 'User created',
						data: userCreate.toJSON()
					}
					return response.send(data)
				} catch (err) {
					trx.rollback()
					
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4003',
						message: 'User not created',
						data: []
					}
					return response.send(data)
				}
			}
		}
	}

	async getLogout({ request, response, auth }) {
		console.log('>>>>>>>>> Masuk Sini <<<<<<<<<<<<<')
		await auth.logout()
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '2000',
			message: 'User successfull logout',
			data: []
		}
		return response.send(data)
	}

	async postForgot({ request, response, auth, session }) {
		const { email, telephone } = request.all()
		
		let user = await User.query().where('email', email).orWhere('telephone', telephone).first()
		
		if (user) {
			let forgetkey = Encryption.createHash('md5').update(user.password).digest("hex")
			
			await User.query().where('email', email).update({
				forget_key: forgetkey
			})
			
			let updateUser = await User.query().where('email', email).first()
			
			// try {
			// 	await Mail.send('emails.forgot', { data: updateUser }, (message) => {
			// 		message.to(email).from(Env.get('MAIL_USERNAME'), 'GoApotik').subject('GoApotik User Forgotten')
			// 	})
			// } catch (err) {}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User successfull send email forgoted',
				data: {}
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User not found',
				data: {}
			}
			return response.send(data)
		}
	}
}

module.exports = AccountController