'use strict'

const User = use('App/Models/User')
const Customer = use('App/Models/Customer')
const Card = use('App/Models/Card')
const Transaction = use('App/Models/Transaction')
const Token = use('App/Models/Token')
const Hash = use('Hash')
const Database = use('Database')
const moment = require('moment')
const { validate, validateAll, rule } = use('Validator')
const Mail = use('Mail')
const Encryption = require('crypto')
const Logger = use('Logger')
const ApiService = require('../Helper/ApiService.js')
const ArangoService = require('../Helper/ArangoService.js')
const couponCodes = require('../Helper/CodeGenerator.js')
const Env = use('Env')

class UserController {
	async getCardList({ params, request, response, auth, session }) {
		let cards = await Card.query().where('user_id', params.id).fetch()

		let cardjson = cards.toJSON()
		console.log(cardjson)

	 	if (cards) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Cards found',
				data: cardjson
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Cards not found',
				data: {}
			}
			return response.send(data)
		}
	}

	async postAddCard({ request, response, auth, session }) {
		let create_date = moment()
		const {
			card_number,
			user_id,
			issuer_id
		} = request.only([
			'card_number',
			'user_id',
			'issuer_id',
			'principal_id'
		])

		let formData = {
			user_id: user_id,
			issuer_id: issuer_id,
			card_number: card_number,
			principal_id: principal_id,
			created_by: user_id,
			updated_by: user_id,
			created_at: create_date.format('YYYY-MM-DD hh:mm:ss'),
			updated_at: create_date.format('YYYY-MM-DD hh:mm:ss'),			
		}

		const rules = {
			card_number: 'required',
		}
		
		const validation = await validate(formData, rules)
		
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: validation.messages(),
				data: []
			}
			return response.send(data)
		} else {
			const trx = await Database.beginTransaction()
			
			try {
				let card = await Card.create(formData, trx)
				
				trx.commit()

				ArangoService.generatePointByAcountId(
					{
						principalId : formData.principal_id,
						cardNumber : formData.card_number,
						issuerId : formData.issuer_id
					}
				)

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Card added',
					data: card
				}
				return response.send(data)
			} catch (err) {
				trx.rollback()
				console.log(err)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: 'Card not created',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async getTransactionList({ params, request, response, auth, session }) {
		let transactions = await Transaction.query().where('user_id', params.id).limit(3).fetch()

		let transactionjson = transactions.toJSON()
		console.log(transactionjson)

	 	if (cards) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Transaction found',
				data: transactionjson
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Transaction not found',
				data: {}
			}
			return response.send(data)
		}
	}

	async postPassword({ request, response, auth, session }) {
		const {
			new_password,
			old_password,
			confirm_password,
			user_id
		} = request.only([
			'new_password',
			'old_password',
			'confirm_password',
			'user_id'
		])
		console.log('.>>>>>>>>>sini')

		let user = await User.query().where('id', user_id).first()

		let formData = {
			password: await Hash.make(new_password),
			updated_by: user_id,
		}

		const rules = {
			password: 'required',
		}
		
		const validation = await validate(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: validation.messages(),
				data: {}
			}
			return response.send(data)
		} else {
			if(await Hash.verify(old_password, user.password) == true) {
				await User.query().where('id', user_id).update(formData)

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Password successfull updated',
					data: {}
				}
				
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Password not found',
					data: {}
				}
				return response.send(data)
			}
			
		}
	}
}

module.exports = UserController