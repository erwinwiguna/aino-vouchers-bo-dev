'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PrincipalsSchema extends Schema {
  up () {
    this.create('principals', (table) => {
    	table.increments()
		table.integer('user_id')
		table.string('name')
		table.string('principal_code')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('principals')
  }
}

module.exports = PrincipalsSchema
