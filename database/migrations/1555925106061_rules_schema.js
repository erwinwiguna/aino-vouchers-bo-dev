'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RulesSchema extends Schema {
  up () {
    this.create('rules', (table) => {
    	table.increments()
		table.string('name')
		table.string('activity_type')
		table.string('spending_type')
		table.integer('spending_amount')
		table.string('reward_type')
		table.integer('reward_amount')
		table.datetime('period_start')
		table.datetime('period_end')
		table.string('issuer_id')
		table.string('customer')
		table.integer('status')
		table.string('principal_id')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('rules')
  }
}

module.exports = RulesSchema
