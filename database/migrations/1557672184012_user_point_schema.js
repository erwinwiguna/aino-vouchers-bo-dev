'use strict'

const Schema = use('Schema')

class UserPointSchema extends Schema {
  up () {
    this.create('user_points', (table) => {
	    table.increments()
	    table.integer('user_id')
		table.integer('point')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
      	table.timestamps()
    })
  }

  down () {
    this.drop('user_points')
  }
}

module.exports = UserPointSchema
