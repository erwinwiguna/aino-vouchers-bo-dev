'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CardsSchema extends Schema {
  up () {
    this.create('cards', (table) => {
      table.increments()
  		table.integer('user_id')
  		table.integer('issuer_id')
  		table.string('card_number')
      table.string('principal_id')
  		table.integer('created_by', 10).notNullable()
  		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('cards')
  }
}

module.exports = CardsSchema
