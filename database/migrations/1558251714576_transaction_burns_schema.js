'use strict'

const Schema = use('Schema')

class TransactionBurnsSchema extends Schema {
	up () {
		this.create('transaction_burns', (table) => {
			table.increments()
			table.integer('user_id')
			table.string('user_name')
			table.integer('issuer_id')
			table.string('issuer_name')
			table.integer('card_id')
			table.string('card_number')
			table.integer('reward_id')
			table.integer('reward_used')
			table.string('reward_name')
			table.datetime('date')
			table.string('status')
			table.string('activity_type')
			table.text('response')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('transaction_burns')
	}
}

module.exports = TransactionBurnsSchema
