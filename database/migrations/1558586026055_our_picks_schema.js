'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OurPickSchema extends Schema {
  up () {
    this.create('our_picks', (table) => {
      table.increments()
      table.integer('user_id')
      table.string('principal_id')
      table.string('principal_code')
      table.integer('reward_id')
      table.datetime('date_start')
      table.datetime('date_end')
      table.integer('status')
      table.integer('created_by', 10).notNullable()
      table.integer('updated_by', 10).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('our_picks')
  }
}

module.exports = OurPickSchema
