'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RewardsSchema extends Schema {
  up () {
    this.create('rewards', (table) => {
    	table.increments()
		table.integer('loyalty_detail_id')
		table.string('name')
		table.text('term_condition')
		table.string('location')
		table.string('quantity')
		table.string('point_required')
		table.integer('status')
		table.string('category_id')
		table.string('category_name')
		table.string('images')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('rewards')
  }
}

module.exports = RewardsSchema
