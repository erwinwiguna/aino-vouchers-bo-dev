'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomersSchema extends Schema {
  up () {
    this.create('customers', (table) => {
    	table.increments()
		table.integer('user_id')
		table.string('gender')
		table.integer('phone_number')
		table.date('date_of_birth')
		table.string('identity_number')
		table.string('address')
		table.string('image')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomersSchema
