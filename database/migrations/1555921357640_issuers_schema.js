'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IssuersSchema extends Schema {
  up () {
    this.create('issuers', (table) => {
    	table.increments()
		table.string('name')
		table.string('description')
		table.string('image')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
    	table.timestamps()
    })
  }

  down () {
    this.drop('issuers')
  }
}

module.exports = IssuersSchema
