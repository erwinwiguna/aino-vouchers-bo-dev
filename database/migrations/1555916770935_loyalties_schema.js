'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LoyaltiesSchema extends Schema {
  up () {
	this.create('loyalties', (table) => {
		table.increments()
		table.string('principal_id')
		table.string('merchant_id')
		table.string('principal_name')
		table.string('merchant_name')
		table.integer('status')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
		table.timestamps()
	})
  }

  down () {
	this.drop('loyalties')
  }
}

module.exports = LoyaltiesSchema
