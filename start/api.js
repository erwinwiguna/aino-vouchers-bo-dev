'use strict'

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Http api routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.group(() => {
	Route.get('/', 'Api/ApiController.index').formats(['json'])

	Route.post('login', 'Api/AccountController.postLogin').formats(['json'])
	Route.post('loginsocial', 'Api/AccountController.postLoginSocial').formats(['json'])
	Route.get('logout', 'Api/AccountController.getLogout').formats(['json'])
	Route.post('register', 'Api/AccountController.postRegister').formats(['json'])
	Route.post('forgot', 'Api/AccountController.postForgot').formats(['json'])

	Route.get('getcardlist/:id', 'Api/UserController.getCardList').formats(['json'])
	Route.post('postaddcard', 'Api/UserController.postAddCard').formats(['json'])
	Route.post('postpassword', 'Api/UserController.postPassword').formats(['json'])

	Route.get('gettransactionlist/:id', 'Api/UserController.getTransactionList').formats(['json'])

	Route.get('getsetting', 'Api/ApiController.getSetting').formats(['json'])
	Route.get('getsettingbyid/:id', 'Api/ApiController.getSettingById').formats(['json'])
})
.prefix('api/v1')
.middleware('globalparam')